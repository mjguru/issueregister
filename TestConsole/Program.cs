﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.BusinessObjectModel;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = ServiceFactory.GetService();
            var user = service.GetUserById(new GetUserRequest() { UserId = 1 });

            Console.WriteLine("Success, the user name is: " + user.User.Firstname + " " + user.User.Lastname);
            Console.ReadKey();
            
        }
    }
}
