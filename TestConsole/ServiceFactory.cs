﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.Service.Interface;
using ireg.Service.Implementation;

namespace TestConsole
{
    public class ServiceFactory
    {
        public static IRegService GetService()
        {
            return new RegService();

        }
    }
}
