﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.Common.Log;
using ireg.Common.Utilities;

namespace ireg.BusinessLogic.Base
{
    public class BuisnessLogicLogger : IBuisnessLogicLogger
    {
       

        public BuisnessLogicLogger()
        {

        }

        public void LogInfo(string message)
        {
            LogHelper.Logger.LogInfo(CreateLogData(new
            {
                Message = message
            }));
        }

        public void LogError(string message)
        {
            LogHelper.Logger.LogError(CreateLogData(new
            {
                Message = message
            }));
        }

        public void LogException(Exception exception, string details = null)
        {
            LogHelper.Logger.LogError(CreateLogData(new
            {
                Message = exception.GetType().Name,
                Stacktrace = exception.GetExceptionStackTraceLines(),
                Details = details
            }));
        }

        public void LogProcessRequestStarted()
        {
             
        }

        public void LogProcessRequestFinished()
        {
            
        }

        private LogData CreateLogData(object logObject)
        {
            return new LogData();
        }
    }
}
