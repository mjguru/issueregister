﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using ireg.Repository.Base;
using ireg.Infrastructure.Interface;
using ireg.Infrastructure.Implementation;
using ireg.BusinessObjectModel;
using ireg.Common;
using ireg.Common.Utilities;

namespace ireg.BusinessLogic.Base
{
    public class BusinessLogicBase
    {
        private IRepositoryContext _repositoryContext;
        private BaseResponse _response;

        protected TResponse ProcessRequest<TResponse>(RequestMetadata<TResponse> metadata) where TResponse : BaseResponse, new()
        {
            using (var uow = new UnitOfWork())
            { 

                _response = new BaseResponse();
                _repositoryContext = new RepositoryContext(uow);

                try
                {

                    // CreateSecurityContext(metadata.Request, metadata.AuthRequired, metadata.AllowedAuthModelTypes);
                    // ValidateRequest(metadata.Request);
                    CreateResponse(metadata);


                }
                catch (BaseException baseException)
                {
                    EnsureResponseCreated<TResponse>();
                    HandleBaseException(baseException);
                    //RunActionSafely(metadata.BuisnessLogicExceptionAction);
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException validationException)
                {
                    EnsureResponseCreated<TResponse>();
                    HandleValidationException(validationException);
                    //RunActionSafely(metadata.GeneralExceptionAction);
                }
                catch (Exception generalException)
                {
                    EnsureResponseCreated<TResponse>();
                    HandleGeneralException(generalException);
                    //RunActionSafely(metadata.GeneralExceptionAction);
                }
                finally
                {
                    //RunActionSafely(metadata.FinallyAction);
                }

            }

            _repositoryContext = null;
            return (TResponse)_response;
        }

        private void CreateResponse<TResponse>(RequestMetadata<TResponse> metadata) where TResponse : BaseResponse, new()
        { 

            if (metadata.LogicAction != null)
            {
                _response = metadata.LogicAction(_repositoryContext);
                _repositoryContext.UoW.Save();                
            }

            EnsureResponseCreated<TResponse>();

            _response.Success = true;
        }

        private void EnsureResponseCreated<TResponse>() where TResponse : BaseResponse, new()
        {
            if (_response == null)
            {
                _response = new TResponse();
            }
        }

        private void HandleBaseException(BaseException baseException)
        {
            HandleException(baseException, baseException.ErrorCode, null, baseException.Details);
        }

        private void HandleValidationException(System.Data.Entity.Validation.DbEntityValidationException validationException)
        {
            var errorMessages = validationException.EntityValidationErrors
                                                   .SelectMany(x => x.ValidationErrors)
                                                   .Select(x => String.Format("{0}, property name = {1}", x.ErrorMessage, x.PropertyName))
                                                   .ToList();
            var exceptionMessage = String.Concat(validationException.Message, " The validation errors are: ", String.Join("; ", errorMessages));
            HandleException(validationException, Error.ErrorCode.Unknown, exceptionMessage);
        }

        private void HandleGeneralException(Exception exception)
        {
            HandleException(exception, Error.ErrorCode.Unknown);
        }

        private void HandleException(Exception exception, Error.ErrorCode errorCode, string additionalMessageToLog = null, string businessLogicErrorDetails = null)
        {
            //BuisnessLogicContext.Logger.LogException(exception, additionalMessageToLog);
            //var response = BuisnessLogicContext.Response;
            //response.ErrorCode = (int)errorCode;
            //response.ErrorMessage = Error.GetErrorMessage(errorCode, businessLogicErrorDetails);
            //if (errorCode == Error.ErrorCode.SecurityErrorTokenNotFound)
            //{
            //    throw exception;
            //}
        }

        protected void RunActionSafely(Action action)
        {
            try
            {
                if (action != null)
                {
                    action();
                }
            }
            catch (Exception e)
            {
                HandleGeneralException(e);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static TransactionScope CreateTransactionScope()
        {
            return CreateTransactionScope(TransactionScopeOption.Required);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionScopeOption"></param>
        /// <returns></returns>
        public static TransactionScope CreateTransactionScope(TransactionScopeOption transactionScopeOption)
        {
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted
            };

            return new TransactionScope(transactionScopeOption, transactionOptions);
        }
    }
}
