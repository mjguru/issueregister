﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ireg.BusinessLogic.Base
{
    public interface IBuisnessLogicLogger
    {
        void LogInfo(string message);
        void LogError(string message);
        void LogException(Exception exception, string details = null);
        void LogProcessRequestStarted();
        void LogProcessRequestFinished();
    }
}
