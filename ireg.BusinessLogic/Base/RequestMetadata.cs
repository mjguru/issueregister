﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.BusinessObjectModel;
using ireg.Repository.Base;

namespace ireg.BusinessLogic.Base
{
    public class RequestMetadata<TResponse> where TResponse : BaseResponse
    {
        public BaseRequest Request { get; private set; }
        public Func<IRepositoryContext, TResponse> LogicAction { get; set; }
        
        public RequestMetadata(BaseRequest request)
        {
            Request = request;
            
        }
    }
}
