﻿using ireg.Infrastructure.Implementation;
using ireg.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.BusinessLogic.Base;
using ireg.BusinessLogic.Interface;
using ireg.Repository.Implementation;
using ireg.Repository.Interface;
using ireg.Repository.Base;
using ireg.BusinessObjectModel;
using AutoMapper;

namespace ireg.BusinessLogic.Implementation.User
{
    public class UserLogic 
    {
        private IUserRepository repository;

        public UserLogic(IRepositoryContext context)
        {            
             repository = new UserRepository(context);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetUserResponse GetUserById(GetUserRequest request)
        {
            //Mapper.Map<Employee, Person>()
            //.ForMember(dest => dest.FName, opt => opt.MapFrom(src => src.FirstName))
            //.ForMember(dest => dest.LName, opt => opt.MapFrom(src => src.LastName));

            var response = new GetUserResponse();

            var userObject = repository.GetUserById(request.UserId);
            if (userObject != null)
            {
                response.User = new BusinessObjectModel.User()
                {
                    Firstname = userObject.Firstname,
                    Lastname = userObject.Lastname
                };          
            }

            response.Success = true;
            
            return response;
        }
    }
}
