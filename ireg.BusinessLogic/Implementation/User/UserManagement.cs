﻿using ireg.Infrastructure.Implementation;
using ireg.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.BusinessLogic.Base;
using ireg.BusinessLogic.Interface;
using ireg.BusinessObjectModel;
using ireg.Repository.Implementation;

namespace ireg.BusinessLogic.Implementation.User
{
    public class UserManagement : BusinessLogicBase, IUserManagement
    {
        public GetUserResponse GetUserById(GetUserRequest request)
        {
            var processRequestMetadata = new RequestMetadata<GetUserResponse>(request);
            processRequestMetadata.LogicAction = context => new UserLogic(context).GetUserById(request);            
            return ProcessRequest(processRequestMetadata);
        }

         
    }
}
