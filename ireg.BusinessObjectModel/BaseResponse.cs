﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ireg.BusinessObjectModel
{
    public class BaseResponse
    {
        public enum GENERAL_ERROR
        {
            OK = 200,
            CONNECTION_TIMEOUT = 110,
            UNAUTHORIZED = 401,
        }

        public GENERAL_ERROR? ErrorNo { get; set; }

        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public int ErrorCode { get; set; }

        public BaseResponse()
        {
            Success = false;
        }
    }
}
