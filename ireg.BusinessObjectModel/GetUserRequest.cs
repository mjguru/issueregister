﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ireg.BusinessObjectModel
{
    public class GetUserRequest : BaseRequest
    {
        public int UserId { get; set; }
    }
}
