﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 

namespace ireg.BusinessObjectModel
{
    public class GetUserResponse : BaseResponse
    {
        public User User { get; set; }
    }
}
