﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ireg.BusinessObjectModel
{
    public class User
    {
        public int Id { get; set; }

        public Guid Identifier { get; set; } 

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Username { get; set; }
         
    }
}
