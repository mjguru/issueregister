﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.Common.Utilities;

namespace ireg.Common.Enum
{
    public enum Role
    {  
        [EnumDisplayName("Admin")]
        Admin = 1,

        [EnumDisplayName("Customer")]
        Customer = 2,

        [EnumDisplayName("Company")]
        Company = 3

    }
}
