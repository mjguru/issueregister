﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ireg.Common
{
    public class Error
    {
        public enum ErrorCode
        {
            Unknown = 0,
            NotFound = 1

        }

        private static readonly Dictionary<int, string> ErrorMessages = new Dictionary<int, string>
        {
            {(int)ErrorCode.Unknown, "Internal Server Error"},
             {(int)ErrorCode.NotFound, "No Results Found"},              

        };

        public static string GetErrorMessage(ErrorCode code, string details)
        {
            var message = ErrorMessages[(int)code];
            if (!String.IsNullOrEmpty(details))
            {
                message = String.Format("{0} {1}", message, details);
            }
            return message;
        }
    }
}
