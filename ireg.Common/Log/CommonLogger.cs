﻿using System;
using System.Globalization;
using System.Text;
using NLog;
using ireg.Common.Utilities;

namespace ireg.Common.Log
{
    internal class CommonLogger : ILogger
    {
        private const int ColumnSize = 15;

        private static Logger Logger { get; set; }

        static CommonLogger()
        {
            Logger = LogManager.GetCurrentClassLogger();
        }

        public void LogInfo(LogData logData)
        {
            Log(logData, LogLevel.Info);
        }

        public void LogWarning(LogData logData)
        {
            Log(logData, LogLevel.Warn);
        }

        public void LogError(LogData logData)
        {
            Log(logData, LogLevel.Error);
        }

        private void Log(LogData logData, LogLevel logLevel)
        {
            var messageBuilder = new StringBuilder();

            messageBuilder.AppendLine();

            AppendLogDataProperty(messageBuilder, "RequestId", logData.RequestId);
            AppendLogDataProperty(messageBuilder, "UserName", logData.UserName ?? "null");
            //AppendLogDataProperty(messageBuilder, "AuthToken", logData.AuthToken.HasValue ? logData.AuthToken.Value.ToString() : "null");
            AppendLogDataProperty(messageBuilder, "ModuleName", logData.ModuleName);
            AppendLogDataProperty(messageBuilder, "MethodName", logData.MethodName);
            AppendLogDataProperty(messageBuilder, "Details", logData.LogObject.ToLog(ColumnSize));

            messageBuilder.AppendLine();

            Logger.Log(logLevel, CultureInfo.CurrentCulture, messageBuilder.ToString());
        }

        private void AppendLogDataProperty(StringBuilder messageBuilder, string propertyName, object propertyValue)
        {
            var propertyNameEx = String.Format("{0}:", propertyName).PadRight(ColumnSize);
            messageBuilder.AppendLine();
            messageBuilder.AppendFormat("{0}{1}", propertyNameEx, propertyValue);
        }
    }

}
