﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ireg.Common.Log
{
    public interface ILogger
    {
        void LogInfo(LogData logData);
        void LogWarning(LogData logData);
        void LogError(LogData logData);
    }
}
