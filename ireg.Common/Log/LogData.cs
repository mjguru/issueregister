﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ireg.Common.Log
{
    public class LogData
    {
        public Guid? RequestId { get; set; }
        public string UserName { get; set; }
        public Guid? AuthToken { get; set; }
        public string ModuleName { get; set; }
        public string MethodName { get; set; }
        public object LogObject { get; set; }
    }
}
