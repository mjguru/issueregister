﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ireg.Common.Utilities
{
    public abstract class BaseException : Exception
    {
        public Error.ErrorCode ErrorCode { get; private set; }
        public string Details { get; private set; }

        protected BaseException(Error.ErrorCode code)
        {
            ErrorCode = code;
        }

        protected BaseException(Error.ErrorCode code, string details)
            : this(code)
        {
            Details = details;
        }

        public override string Message
        {
            get
            {
                return String.Format("{0}, errorCode = {1}, errorMessage = {2}", GetType().Name, (int)ErrorCode, Error.GetErrorMessage(ErrorCode, Details));
            }
        }
    }
}
