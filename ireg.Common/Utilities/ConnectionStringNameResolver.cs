﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ireg.Common.Utilities
{
    public class ConnectionStringNameResolver
    {
        private const string DefaultConnectionStringName = "iregConnectionString";

        public static string ConnectionStringName
        {
            get { return DefaultConnectionStringName; }
        }
    }
}
