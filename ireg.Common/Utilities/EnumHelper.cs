﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ireg.Common.Utilities
{
    public class EnumHelper
    {
        public static TEnum ParseIntToEnum<TEnum>(int value)
        {
            var enumValues = System.Enum.GetValues(typeof(TEnum));
            foreach (var enumValue in enumValues)
            {
                if ((int)enumValue == value)
                {
                    return (TEnum)enumValue;
                }
            }
            throw new NotSupportedException();
        }

        public static string GetEnumNameValue<TEnum>(TEnum enumValue) where TEnum : struct
        {
            return EnumMetadata<TEnum, EnumDisplayNameAttribute>.GetValue(enumValue);
        }
    }
}
