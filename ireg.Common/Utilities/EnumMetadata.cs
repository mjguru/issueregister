﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ireg.Common.Utilities
{
    public class EnumMetadata<TEnum, TAttribute>
        where TEnum : struct
        where TAttribute : EnumBaseAttribute
    {
        public static string GetValue(TEnum t)
        {
            var type = CheckType();
            var memInfo = type.GetMember(t.ToString());
            var value = t.ToString();
            if (memInfo.Length > 0)
            {
                var attributes = memInfo[0].GetCustomAttributes(typeof(TAttribute), false);
                if (attributes.Length > 0)
                {
                    value = ((EnumBaseAttribute)attributes[0]).Value;
                }
            }

            return value;
        }

        private static Type CheckType()
        {
            var type = typeof(TEnum);

            if (type.BaseType != typeof(System.Enum)) throw new ArgumentException("T must be an enumeration.");

            return type;
        }
    }

    public abstract class EnumBaseAttribute : Attribute
    {
        public abstract string Value { get; }
    }

    public class EnumDisplayNameAttribute : EnumBaseAttribute
    {
        private readonly string _key;

        public EnumDisplayNameAttribute(string key)
        {
            _key = key;
        }

        public override string Value
        {
            get
            {
                return _key;
            }
        }
    }

}
