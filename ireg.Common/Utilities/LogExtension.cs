﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ireg.Common.Utilities
{
    public static class LogExtension
    {
        public static string Dump(this object obj, string separator = "; ")
        {
            if (obj == null) return "null";

            var list = obj.GetType().GetProperties().Where(info => info.Name != "Password").Select(info => string.Format("{0}: {1}", info.Name, info.GetValue(obj, null)));

            return string.Format("{0}\r\n{1}", obj.GetType().FullName, string.Join(separator, list));
        }

        public static List<string> GetExceptionStackTraceLines(this Exception exception)
        {
            var messages = new List<string>();

            var exc = exception;
            do
            {
                messages.Add(exc.Message);
                exc = exc.InnerException;
            }
            while (exc != null);

            messages.Add(exception.StackTrace);

            return messages;
        }

        internal static string ToLog(this object obj, int leftPadding = 0)
        {
            if (obj == null) return "null";
            var stringBuilder = new StringBuilder();
            AppendObjectLog(stringBuilder, obj, leftPadding, 1);
            return stringBuilder.ToString();
        }

        private static void AppendObjectLog(StringBuilder stringBuilder, object obj, int padding, int hierarchyLevel)
        {
            bool addedFirstLine = hierarchyLevel != 1;

            var type = obj.GetType();
            var properties = type.GetProperties();

            foreach (var property in properties)
            {
                if (property.Name == "AuthToken" || property.Name == "RequestId" || property.Name == "Password")
                {
                    continue;
                }

                if (addedFirstLine)
                {
                    stringBuilder.AppendLine();
                    stringBuilder.Append("".PadLeft(hierarchyLevel * padding));
                }
                else
                {
                    addedFirstLine = true;
                }

                stringBuilder.Append(property.Name);
                stringBuilder.Append(": ");

                var propertyValue = obj.GetPropValue(property.Name);

                if (propertyValue != null)
                {

                    var list = propertyValue as IList;
                    if (list != null)
                    {
                        if (property.Name == "Stacktrace")
                        {
                            foreach (var line in list)
                            {
                                stringBuilder.AppendLine();
                                stringBuilder.Append(line);
                            }
                        }
                        else
                        {
                            stringBuilder.Append(String.Format("{0} items", list.Count));
                        }
                    }
                    else
                    {
                        var valueType = propertyValue.GetType();
                        var namesps = valueType.Namespace;
                        if (namesps != null
                            && (namesps.Contains("MPSBuisnessInterfaces"))
                            && !(propertyValue is System.Enum))
                        {
                            AppendObjectLog(stringBuilder, propertyValue, padding, hierarchyLevel + 1);
                        }
                        else
                        {
                            stringBuilder.Append(propertyValue);
                        }
                    }
                }
                else
                {
                    stringBuilder.Append("null");
                }
            }
        }

        private static object GetPropValue(this Object obj, String name)
        {
            foreach (var part in name.Split('.'))
            {
                if (obj == null) { return null; }

                var type = obj.GetType();
                var info = type.GetProperty(part);

                if (info == null) { return null; }

                obj = info.GetValue(obj, null);
            }
            return obj;
        }
    }

}
