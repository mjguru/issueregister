namespace ireg.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.lkRole",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Identifier = c.Guid(nullable: false),
                        RoleFK = c.Int(nullable: false),
                        Firstname = c.String(maxLength: 50),
                        Lastname = c.String(maxLength: 50),
                        Username = c.String(maxLength: 50),
                        Password = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        CreatedBy = c.String(),
                        UpdatedOn = c.DateTime(),
                        UpdatedBy = c.String(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.lkRole", t => t.RoleFK)
                .Index(t => t.RoleFK);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.User", "RoleFK", "dbo.lkRole");
            DropIndex("dbo.User", new[] { "RoleFK" });
            DropTable("dbo.User");
            DropTable("dbo.lkRole");
        }
    }
}
