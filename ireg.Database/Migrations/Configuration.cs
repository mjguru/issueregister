namespace ireg.Database.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using ireg.Common.Utilities;
    using ireg.Common.Enum;
    using ireg.Model.Model;
    using ireg.Model.Base;

    internal sealed class Configuration : DbMigrationsConfiguration<iRegEntities>
    {
        /*
        //if any changes are made or creating the db structure for the first time

        Add-Migration -ProjectName ireg.Database -StartupProjectName ireg.Database -ConnectionStringName iregConnectionString
        
        //create database
        Update-Database -ProjectName ireg.Database -StartupProjectName ireg.Database -ConnectionStringName iregConnectionString
        */

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(iRegEntities context)
        {
            populateDictionaries(context);
        }

        private void populateDictionaries(iRegEntities context)
        {
            populateDictinary<lkRole, Role>(context);

        }

        private void populateDictinary<TPoco, TEnum>(iRegEntities context)
            where TPoco : class, IPocoDictionary
            where TEnum : struct
        {
            foreach (TEnum enumValue in (TEnum[])Enum.GetValues(typeof(TEnum)))
            {
                var poco = Activator.CreateInstance<TPoco>();
                poco.Id = Convert.ToInt32(enumValue);
                poco.Name = EnumHelper.GetEnumNameValue(enumValue);
                poco.IsActive = true;
                context.Set<TPoco>().AddOrUpdate(obj => obj.Id, poco);
            }
        }
    }
}
