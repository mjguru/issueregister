﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ireg.Model.ModelConfiguration;
using ireg.Model.Model;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ireg.Database
{
    public class iRegEntities : DbContext
    {
        public iRegEntities() : base("iRegEntities") { }

        public iRegEntities(string connectionStringName)
            : base(connectionStringName)
        {

        }

        //define all tables here
        public DbSet<User> User { get; set; }
        public DbSet<lkRole> lkRole { get; set; }


        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        { 

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new lkRoleConfiguration());
        }
    }
}
