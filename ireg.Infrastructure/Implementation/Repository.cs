﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ireg.Database;
using ireg.Infrastructure.Interface;
using ireg.Model.Base;

namespace ireg.Infrastructure.Implementation
{
    public class Repository<TObject> : IRepository<TObject> where TObject : class, IPocoBase
    {
        protected IUnitOfWork UoW { get; private set; }
        protected DbContext Context { get; private set; }

        public Repository(IUnitOfWork uow, DbContext context)
        {
            UoW = uow;
            Context = context;
        }

        protected DbSet<TObject> DbSet
        {
            get { return Context.Set<TObject>(); }
        }

        public virtual IQueryable<TObject> AsQueryable()
        {
            return DbSet.AsQueryable();
        }

        public void Add(TObject obj)
        {
            RunInternalLogic<TObject, IPocoRemovable>(obj, poco => poco.IsActive = true);
            RunInternalLogic<TObject, IPocoCreatedOn>(obj, poco => poco.CreatedOn = UoW.Now);
            RunInternalLogic<TObject, IPocoCreatedBy>(obj, poco => poco.CreatedBy = UserName);
            RunInternalLogic<TObject, IPocoUpdatedOn>(obj, poco => poco.UpdatedOn = UoW.Now);
            RunInternalLogic<TObject, IPocoUpdatedBy>(obj, poco => poco.UpdatedBy = UserName);
            RunInternalLogic<TObject, IPocoWithUniqueIdentifier>(obj, poco => poco.Identifier = Guid.NewGuid());

            Context.Set<TObject>().Add(obj);
        }

        public void Update(TObject obj)
        {
            RunInternalLogic<TObject, IPocoUpdatedOn>(obj, poco => poco.UpdatedOn = UoW.Now);
            RunInternalLogic<TObject, IPocoUpdatedBy>(obj, poco => poco.UpdatedBy = UserName);
        }

        public void Delete(TObject obj)
        {

            if (obj is IPocoBase)
            {
                var poco = obj as IPocoRemovable;
                poco.IsActive = false;
                Update(obj);
            }
            else
            {
                DbSet.Remove(obj);
            }
        }

        private void RunInternalLogic<TPoco, TInterface>(TPoco poco, Action<TInterface> action)
            where TPoco : class
            where TInterface : class
        {
            var pocoToSave = poco as TInterface;
            if (pocoToSave != null)
            {
                action(pocoToSave);
            }
        }

        private string UserName
        {
            get
            {
                //if (UoW.SecurityContext != null && UoW.SecurityContext.CurrentUserInfo != null)
                //{
                //    return UoW.SecurityContext.CurrentUserInfo.UserName;
                //}
                return "system";
            }
        }
    }
}
