﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.Database;
using ireg.Infrastructure.Interface;
using ireg.Model.Base;
using System.Data.Entity;
using ireg.Common.Utilities;

namespace ireg.Infrastructure.Implementation
{
    public class UnitOfWork : IUnitOfWork
    { 
        public DateTime Now { get; private set; } 

        private DbContext _dbContext;
        protected DbContext DbContext
        {
            get
            {
                if (_dbContext == null)
                {
                    _dbContext = new iRegEntities(ConnectionStringNameResolver.ConnectionStringName);
                }
                return _dbContext;
            }
        }

        public UnitOfWork()
        {
            Now = DateTime.Now; 
        }

        public UnitOfWork(DbContext context)
            : this()
        {
            _dbContext = context;
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
                _dbContext = null;
            }
            GC.SuppressFinalize(this);
        }

        public virtual void Save()
        { 
            DbContext.SaveChanges();
        }

       // public ISecurityContext SecurityContext { get; set; }

        public virtual IRepository<T> Repository<T>() where T : class, IPocoBase
        {
            return new Repository<T>(this, DbContext);
        }

         
    }
}
