﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ireg.Infrastructure.Interface
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> AsQueryable();
        void Add(T obj);
        void Update(T obj);
        void Delete(T obj);
    }
}
