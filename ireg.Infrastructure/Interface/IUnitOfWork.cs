﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.Model.Base;

namespace ireg.Infrastructure.Interface
{
    public interface IUnitOfWork : IDisposable
    {
        //ISecurityContext SecurityContext { get; set; }
        IRepository<T> Repository<T>() where T : class, IPocoBase;

        void Save();      

        DateTime Now { get; }
        
    }
}
