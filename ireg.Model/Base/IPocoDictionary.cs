﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ireg.Model.Base
{
    public interface IPocoDictionary : IPocoBase, IPocoRemovable
    {
        string Name { get; set; }
         
    }
}
