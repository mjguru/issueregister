﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ireg.Model.Base
{
    public interface IPocoRemovable 
    { 

        bool IsActive { get; set; }
    }
}
