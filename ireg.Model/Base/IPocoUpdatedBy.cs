﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ireg.Model.Base
{
    public interface IPocoUpdatedBy
    {
        string UpdatedBy { get; set; }
    }
}
