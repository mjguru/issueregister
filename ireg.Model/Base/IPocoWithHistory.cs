﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ireg.Model.Base
{
    public interface IPocoWithHistory : IPocoBase, IPocoRemovable, IPocoCreatedOn, IPocoCreatedBy, IPocoUpdatedOn, IPocoUpdatedBy
    {
    }
}
