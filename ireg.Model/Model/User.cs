﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.Model.Base;

namespace ireg.Model.Model
{
    public class User : IPocoWithHistory, IPocoWithUniqueIdentifier
    {
        public int Id { get; set; }

        public Guid Identifier { get; set; }

        public int RoleFK { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public DateTime CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public bool IsActive { get; set; }

        public lkRole lkRole { get; set; }

    }
}
