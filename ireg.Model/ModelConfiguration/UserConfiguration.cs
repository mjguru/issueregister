﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.Model.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace ireg.Model.ModelConfiguration
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ToTable("User");

            HasKey(pkey => pkey.Id);
            Property(c => c.Id).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(c => c.UpdatedBy).IsOptional();
            Property(c => c.UpdatedOn).IsOptional();


            Property(c => c.Firstname).HasMaxLength(50);
            Property(c => c.Lastname).HasMaxLength(50);
            Property(c => c.Username).HasMaxLength(50);

            //foreign key
            HasRequired(t => t.lkRole).WithMany().HasForeignKey(d => d.RoleFK);
        }
    }
}
