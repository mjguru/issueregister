﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.Model.Model;
using System.ComponentModel.DataAnnotations.Schema;

namespace ireg.Model.ModelConfiguration
{
    public class lkRoleConfiguration : EntityTypeConfiguration<lkRole>
    {
        public lkRoleConfiguration()
        {
            ToTable("lkRole");

            HasKey(pkey => pkey.Id);
            Property(c => c.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(c => c.Name).IsRequired().HasMaxLength(50);
      
        }
    }
}
