﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.Infrastructure.Interface;

namespace ireg.Repository.Base
{
    public interface IRepositoryContext
    {
        IUnitOfWork UoW { get; }
    }
}
