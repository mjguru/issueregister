﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.Infrastructure.Interface;
using ireg.Model.Base;

namespace ireg.Repository.Base
{
    public abstract class RepositoryBase
    {
        protected IRepositoryContext RepositoryLogicContext { get; private set; }

        public RepositoryBase(IRepositoryContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            RepositoryLogicContext = context;
        }

        protected IRepository<TPoco> Repository<TPoco>() where TPoco : class, IPocoBase
        {
            return RepositoryLogicContext.UoW.Repository<TPoco>();
        }

        protected IQueryable<TPoco> RepositoryQueryBase<TPoco>() where TPoco : class, IPocoBase
        {
            var query = RepositoryLogicContext.UoW.Repository<TPoco>().AsQueryable();
            return query;
        }

        protected IUnitOfWork UoW
        {
            get { return RepositoryLogicContext.UoW; }
        }
    }
}
