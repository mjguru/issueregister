﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.Infrastructure.Interface;

namespace ireg.Repository.Base
{
    public class RepositoryContext : IRepositoryContext
    {
        private readonly IUnitOfWork _uoW;
        public virtual IUnitOfWork UoW { get { return _uoW; } }

        public RepositoryContext(IUnitOfWork context)
        {
            if (context == null) throw new ArgumentNullException("context");
            _uoW = context;
        }

         
    }
}
