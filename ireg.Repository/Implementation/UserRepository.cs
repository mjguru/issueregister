﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.Infrastructure.Interface;
using ireg.Infrastructure.Implementation;
using ireg.Model.Model;
using ireg.Repository.Base;
using ireg.Repository.Interface;

namespace ireg.Repository.Implementation
{
    public class UserRepository : RepositoryBase, IUserRepository
    {

        public UserRepository(IRepositoryContext context)
            : base(context)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User GetUserById(int id)
        {
            var userQuery = RepositoryQueryBase<User>();
            var lkRoleQuery = RepositoryQueryBase<lkRole>();

            var query = from u in userQuery
                        join r in lkRoleQuery on u.RoleFK equals r.Id
                        where u.Id == id && u.IsActive == true

                        select u;

            return query.FirstOrDefault(); 

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="firstName"></param>
        public void AddUser(int id, string firstName)
        {
            var userRecord = RepositoryQueryBase<User>().Where(u => u.Id == id && u.IsActive == true).FirstOrDefault();
            if (userRecord == null)
            {
                userRecord = new User();
            }

            userRecord.Firstname = firstName.Trim();

            Repository<User>().Add(userRecord);
            UoW.Save();
        }

    }
}
