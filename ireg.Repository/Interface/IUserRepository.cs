﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.Infrastructure.Interface;
using ireg.Infrastructure.Implementation;
using ireg.Model.Model;
using ireg.Repository.Base;

namespace ireg.Repository.Interface
{
    public interface IUserRepository
    {
        User GetUserById(int id);

        void AddUser(int id, string firstName);
    }
}
