﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.Service.Interface;
using ireg.BusinessObjectModel;
using ireg.BusinessLogic.Implementation.User;

namespace ireg.Service.Implementation
{
    public class RegService : IRegService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetUserResponse GetUserById(GetUserRequest request)
        {
            return new UserManagement().GetUserById(request);
        }

    }
}
