﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ireg.BusinessLogic.Implementation.User;
using ireg.BusinessLogic.Interface;
using ireg.BusinessObjectModel;
using System.Collections.Generic;
using NUnit;
using NUnit.Framework;
using ireg.UnitTest.UserManagement;
using System.Linq;
namespace ireg.UnitTest
{
    [TestClass]
    public class UserManagementTest
    {
        GetUserResponse response = new GetUserResponse() { User = new User() { Id = 1, Firstname = "Manu", Lastname = "John" } };

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]    
        public void GetUserByIdTest()
        {
            List<User> userList = new List<User>();
            userList.Add(new User() { Id = 1, Firstname = "Manu", Lastname = "John" });
            
            int userId = 12;
            var mock = new Mock<IUserManagement>();
            //mock.Setup(x => x.GetUserById(It.IsAny<GetUserRequest>())).Returns(response);
            //mock.Setup(t => t.GetUserById(It.Is<GetUserRequest>(p => p.UserId == userId))).Returns(response);

            //mock.Setup(x => x.GetUserById(It.IsAny<GetUserRequest>()))
            //    .Returns<int>(id => userList.SingleOrDefault(r => r.Id == id));


            //system under test
            var sut = new UserManagementTestController(mock.Object);

            var response1 = sut.GetUserById(new GetUserRequest() { UserId = userId });
            NUnit.Framework.Assert.AreEqual(response.User, response1.User,"Failed - result not matching");
            

        }
    }
}
