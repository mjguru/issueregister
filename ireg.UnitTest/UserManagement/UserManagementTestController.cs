﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ireg.BusinessLogic.Implementation.User;
using ireg.BusinessLogic.Interface;
using ireg.BusinessObjectModel;

namespace ireg.UnitTest.UserManagement
{
    
    public class UserManagementTestController
    {
        private IUserManagement _contoller;

        public UserManagementTestController(IUserManagement userManagement)
        {
            _contoller = userManagement;
        }

        public GetUserResponse GetUserById(GetUserRequest request)
        {
            return _contoller.GetUserById(request);
        }


    }
}
